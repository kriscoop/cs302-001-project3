CC = g++
CPPFLAGS = -g -Wall -std=c++17

project2:	cooper-kristian-Project3.o menu.o CMD.o RBT.o animal.o
	g++ cooper-kristian-Project3.o menu.o CMD.o RBT.o animal.o -o project3

cooper-kristian-Project2.o:	cooper-kristian-Project2.cpp
	$(CC) $(CPPFLAGS) -c cooper-kristian-Project3.cpp 

menu.o:		menu.cpp
	$(CC) $(CPPFLAGS) -c menu.cpp 

CMD.o:		CMD.cpp
	$(CC) $(CPPFLAGS) -c CMD.cpp

RBT.o:		RBT.cpp
	$(CC) $(CPPFLAGS) -c RBT.cpp

animal.o:	animal.cpp
	$(CC) $(CPPFLAGS) -c animal.cpp

clean:
	$(info -- cleaning the direcotry --)
	rm *.o
	rm project3
