/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: menu.cpp
Description: 	Here is the implementation for menu class which acts as a driver for user interaction with the RBT.



*/

#include "menu.h"

Menu :: Menu(std::istream & input)
{
	tree.loadTree(input);
}

Menu :: Menu(const RBT & tree): tree(tree)
{
}

int Menu :: run()
{
	char option;
	topMenuSelect(option);
	return 0;
}

int Menu :: loadTree(std::istream & input)
{
	if(input)
	{
		tree.removeAll();
		return tree.loadTree(input);
	}
	return 0;
}

///////////// TOP MENU ////////////////

void Menu :: printTopMenu() const
{
	std::cout << "/-------------- Top Menu: --------------/" << std::endl;
	std::cout << "A) Display" << std::endl;
	std::cout << "B) Print Tree Structure" << std::endl;
	std::cout << "C) Retrieve" << std::endl;
	std::cout << "D) Insert" << std::endl;
	std::cout << "E) RemoveAll" << std::endl;
	std::cout << "F) Load" << std::endl;
	std::cout << "Q) Quit" << std::endl;
	std::cout << "Selection: ";
}

void Menu :: topMenuSelect(char & option)
{
	do
	{
		printTopMenu();
		option = getChar();
		std::cout << std::endl;
		topMenuRead(option);
	}
	while(option != 'q');
}

void Menu :: topMenuRead(char & option)
{
	switch(option)
	{
		case 'a':
			std::cout << "Displaying tree..." << std::endl;
			tree.display();
			break;
		case 'b':
			std::cout << "Printing tree..." << std::endl << tree;
			break;
		case 'c':
			retrievalProcess(option);
			break;
		case 'd':
			insertMenuSelect(option);
			break;
		case 'e':
			removeAllMenu(option);
			break;
		case 'f':
			loadAnimalsMenu();
			break;
		case 'q':
			break;
		default:
			std::cout << "Invalid option..." << std::endl;
			break;
	}	
}


//////////// Create Heirarchy objects ///////
void Menu :: createCat(Animal * & temp, char & option)
{
	std::string name, trait;
	bool temper, ouch;
	int play, care, age, weight, max, size;

	std::cout << "/------------ Creating Cat ------------/\n";
	temp = nullptr;
	
	std::cout << "Enter cat's name: ";
	name = getText();
	
	std::cout << "Does this cat not play well with others? (y/n): ";
	temper = yesNo(option);
	
	std::cout << "Does cat tend to scratch? (y/n): ";
	ouch = yesNo(option);
	
	std::cout << "How many hours has cat played today?: ";
	play = getInt();

	std::cout << "How much care does cat need? (1 least - 10 most): ";
	care = getInt(1, 10);

	std::cout << "Which age group does cat belong to? (1 youngest - 4 oldest): ";
	age = getInt(1, 4);

	std::cout << "How much does this cat weigh? (1 - 50): ";
	weight = getInt(1, 50);
	
	std::cout << "Enter maximun hours cat likes to play: ";
	max = getInt();

	std::cout << "Enter cat's size (1 smallest - 3 largest): ";
	size = getInt(1, 3);

	temp = new Cat{name, temper, play, care, age, weight, ouch, max, size};

	std::cout << "Would you like to add physical Characteristics? (y/n): ";
	while(yesNo(option) == true)
	{
		std::cout << "Enter trait: ";
		trait = getText();
		temp->addTrait(trait);
		std::cout << "Continue? (y/n)";
	}

	std::cout << name << " has been created!" << std::endl;
}

void Menu :: createDog(Animal * & temp, char & option)
{
	std::string name, trait;
	bool temper, good, flea = false, potty = false;
	int play, care, age, energy;

	std::cout << "/------------ Creating Dog ------------/\n";
	temp = nullptr;
	
	std::cout << "Enter dog's name: ";
	name = getText();
	
	std::cout << "Does this dog not play well with others? (y/n): ";
	temper = yesNo(option);
	
	std::cout << "How many hours has dog played today?: ";
	play = getInt();

	std::cout << "How much care does dog need? (1 least - 10 most): ";
	care = getInt(1, 10);

	std::cout << "Which age group does dog belong to? (1 youngest - 4 oldest): ";
	age = getInt(1, 4);

	std::cout << "Is dog typically a good boy? (y/n): ";
	good = yesNo(option);

	std::cout << "How much energy does dog have? (1 least - 10 most): ";
	energy = getInt(1, 10);

	temp = new Dog{name, temper, play, care, age, good, flea, potty, energy};

	std::cout << "Would you like to add physical Characteristics? (y/n): ";
	while(yesNo(option) == true)
	{
		std::cout << "Enter trait: ";
		trait = getText();
		temp->addTrait(trait);
		std::cout << "Continue? (y/n)";
	}

	std::cout << "Would you like to any special notes? (y/n): ";
	Dog * dog = static_cast<Dog *>(temp);
	while(yesNo(option) == true)
	{
		std::cout << "Enter trait: ";
		trait = getText();
		dog->addNotes(trait);
		std::cout << "Continue? (y/n)";
	}

	std::cout << name << " has been created!" << std::endl;
}

void Menu :: createFish(Animal * & temp, char & option)
{
	std::string name, trait;
	bool temper, fresh, hunger = true;
	int play, care, age, tank = 10, disease = 1, temperature;

	std::cout << "/------------ Creating Fish ------------/\n";
	temp = nullptr;
	
	std::cout << "Enter fish's name: ";
	name = getText();
	
	std::cout << "Does this fish not play well with others? (y/n): ";
	temper = yesNo(option);
	
	std::cout << "How many hours has fish played today?: ";
	play = getInt();

	std::cout << "How much care does fish need? (1 lease - 10 most): ";
	care = getInt(1, 10);

	std::cout << "Which age group does fish belong to? (1 youngest - 4 oldest): ";
	age = getInt(1, 4);

	std::cout << "Is this a freshwater fish? (y - freshwater/n - saltwater): ";
	fresh = yesNo(option);
	
	std::cout << "Enter the preferred water temperature (1 coolest - 4 warmest): ";
	temperature = getInt(1, 4);
	
	temp = new Fish{name, temper, play, care, age, fresh, hunger, tank, disease, temperature};

	std::cout << "Would you like to add physical Characteristics? (y/n): ";
	while(yesNo(option) == true)
	{
		std::cout << "Enter trait: ";
		trait = getText();
		temp->addTrait(trait);
		std::cout << "Continue? (y/n)";
	}

	std::cout << name << " has been created!" << std::endl;
}

////////////// Retrieve Node and Manipulate ///////////////////
void Menu :: retrievalProcess(char & option)
{
	std::string key;
	std::cout << "Search by Animal name. Enter name: ";
	key = getText();
	
	Animal * result = tree.retrieve(key);

	if(result)
	{
		std::cout << "Node found!\n" << std::endl;
		retrieveMenuSelect(result, option);	
	}
	else
		std::cout << "Sorry, no search results for \"" << key << "\" found.\n" << std::endl;
}
	
///////////// Downcasting to print correct menu //////////////////
void Menu :: printRetrieveMenu(Animal * & animal) const
{
	Cat * aCat = dynamic_cast<Cat *>(animal);
	Dog * aDog = dynamic_cast<Dog *>(animal);
	Fish * aFish = dynamic_cast<Fish *>(animal);
	
	if(aCat)
		printCatMenu();
	else if(aDog)
		printDogMenu();
	else if(aFish)
		printFishMenu();
	else
		std::cout << "Error, no matching Derived class found." << std::endl;	
}

void Menu :: retrieveMenuSelect(Animal * & animal, char & option)
{
	do
	{
		printRetrieveMenu(animal); 
		option = getChar();
		std::cout << std::endl;
		retrieveMenuRead(animal, option);
	}
	while(option != 'q' && option != '<' && animal);
	if(option == '<')
		option = 'a';
}

///////////// Will read the correct option. Here there is Upcasting and virtual functions at a - e.
///////////// Special functions appear in default case
void Menu :: retrieveMenuRead(Animal * & animal, char & option)
{
	int play = 0;
	switch(option)
	{
		case 'a':
			std::cout << *animal << "\n"; 
			break;
		case 'b':
			std::cout << "Enter the number of hours to play (1 - 6): ";
			play = getInt(1, 6);
			animal->play(play);
			break;
		case 'c':
			animal->rest();
			break;
		case 'd':
			animal->checkDisposition();
			break;
		case 'e':
			animal->checkHealth();
			break;
		case 'f':
			animal->checkCare();
		case 'q':
			break;
		case '<':
			break;
		default:
			specialtyOptionRead(animal, option);
			break;
	}
	std::cout << std::endl;
}

////////////// Downcasting to send to  appropriate specialty menu
void Menu :: specialtyOptionRead(Animal * & animal, char & option)
{
	if(animal)
	{
		Cat * aCat = dynamic_cast<Cat *>(animal);
		Dog * aDog = dynamic_cast<Dog *>(animal);
		Fish * aFish = dynamic_cast<Fish *>(animal);
		
		if(aCat)
			catSpecialtyRead(aCat, option);
		else if(aDog)
			dogSpecialtyRead(aDog, option);
		else if(aFish)
			fishSpecialtyRead(aFish, option);
	}
}

//////////// Cat Manipulation ////////////
void Menu :: printCatMenu() const
{
	std::cout << "Please select an option:\n";
	std::cout << "A) Display with animal\n";
	std::cout << "B) Play with animal\n";
	std::cout << "C) Let animal rest\n";
	std::cout << "D) Check animal's disposition\n";
	std::cout << "E) Check animal's health\n";
	std::cout << "F) Check animal's care\n";
	std::cout << "G) Give cat treats's\n";
	std::cout << "Q) Quit\n";
	std::cout << "<) Back\n";
	std::cout << "Selection: ";
}

void Menu :: catSpecialtyRead(Cat * animal, char & option)
{
	switch(option)
	{
		case 'g':
			animal->giveTreat();
			break;
		default:
			std::cout << "Invalid option..." << std::endl;
	}
}


//////////// Dog Manipulation ////////////
void Menu :: printDogMenu() const
{
	std::cout << "Please select an option:\n";
	std::cout << "A) Display with animal\n";
	std::cout << "B) Play with animal\n";
	std::cout << "C) Let animal rest\n";
	std::cout << "D) Check animal's disposition\n";
	std::cout << "E) Check animal's health\n";
	std::cout << "F) Check animal's care\n";
	std::cout << "G) Take dog on walk\n";
	std::cout << "H) Give dog flea treatment\n";
	std::cout << "Q) Quit\n";
	std::cout << "<) Back\n";
	std::cout << "Selection: ";
}

void Menu :: dogSpecialtyRead(Dog * animal, char & option)
{
	switch(option)
	{
		case 'g':
			animal->walkDog();
			break;
		case 'h':
			animal->giveTreatment();
			break;
		default:
			std::cout << "Invalid option..." << std::endl;
	}
}


//////////// Fish Manipulation ////////////
void Menu :: printFishMenu() const
{
	std::cout << "Please select an option:\n";
	std::cout << "A) Display with animal\n";
	std::cout << "B) Play with animal\n";
	std::cout << "C) Let animal rest\n";
	std::cout << "D) Check animal's disposition\n";
	std::cout << "E) Check animal's health\n";
	std::cout << "F) Check animal's care\n";
	std::cout << "G) Feed fish flakes\n";
	std::cout << "H) Give fish medicine\n";
	std::cout << "I) Clean fish tank\n";
	std::cout << "Q) Quit\n";
	std::cout << "<) Back\n";
	std::cout << "Selection: ";
}

void Menu :: fishSpecialtyRead(Fish * animal, char & option)
{
	switch(option)
	{
		case 'g':
			animal->feedFlakes();
			break;
		case 'h':
			animal->giveMedicine();
			break;
		case 'i':
			animal->cleanTank();
			break;
		default:
			std::cout << "Invalid option..." << std::endl;
	}
}

//////////// Insert
void Menu :: printInsertMenu() const
{
	std::cout << "Please select an option:\n";
	std::cout << "A) Create a Cat\n";
	std::cout << "B) Create a Dog\n";
	std::cout << "C) Create a Fish\n";
	std::cout << "Q) Quit\n";
	std::cout << "<) Back\n";
	std::cout << "Selection: ";
}

void Menu :: insertMenuSelect(char & option)
{
	do
	{
		printInsertMenu(); 
		option = getChar();
		insertMenuRead(option);
	}
	while(option != 'q' && option != '<');
	if(option == '<')
		option = 'a';
}	

void Menu :: insertMenuRead(char & option)
{
	Animal * animal = nullptr;
	switch(option)
	{
		case 'a':
			createCat(animal, option);
			tree.insert(animal);
			break;
		case 'b':
			createDog(animal, option);
			tree.insert(animal);
			break;
		case 'c':
			createFish(animal, option);
			tree.insert(animal);
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			std::cout << "Invalid option..." << std::endl;
	}
}

////////////// Remove All
void Menu :: removeAllMenu(char & option)
{
	std::cout << "Are you sure you want to remove all? (y/n): ";
	bool result = yesNo(option);
	if(result)
		tree.removeAll();
}

void Menu :: loadAnimalsMenu()
{
	std::ifstream input;
	input.open("animalData.txt");
	if(input)
	{
		tree.removeAll();
		tree.loadTree(input);
	}
}




