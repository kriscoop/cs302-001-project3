#include <fstream>
#include "menu.h"


using namespace std;

int main()
{
	ifstream input;
	input.open("animalData.txt");
	if(input)
	{
		Menu menu(input);
		menu.run();
	}
	else
	{
		cout << "Failed to load from file." << endl;
		Menu menu(input);
		menu.run();
	}
	
/*
	string nameA = "Marley";
	string nameB = "Frank";
	int play = 13, care = 3, age = 3, tank = 6, sickness  = 1, temp = 3;
	bool temper = false, fresh = true, hunger = false;
	string buffer;
	vector<string> list{	"Marvin", "Alfie", "Rainbow", "Marley", "Jenine", "Dianna", "Dori", "Nemo", "Barnabie", 
				"Crumpkin", "King", "Frank", "Sushi", "Anvil", "Lollaby", "Michelle", "Gary", "Ingrid"};

	RBT tree;
	Animal * fish;

	for(string i : list)
	{
		buffer = i;
		fish = new Fish{buffer,temper, play, care, age, fresh, hunger, tank, sickness, temp};	 
		tree.insert(fish);
	}

	tree.display();
	tree.removeAll();

	for(string i : list)
	{
		buffer = i;
		fish = new Fish{buffer,temper, play, care, age, fresh, hunger, tank, sickness, temp};	 
		tree.insert(fish);
	}
	RBT treeB(tree);
	treeB.display();
	string name = "Marvin";
	cout << "Attempting to find " << name << "..." << endl;
	Animal * find = treeB.retrieve(name);
	if(find)
		cout << "Found the following animal\n\n" << *find << endl;
	cout << tree << std::endl << "The End!" << std::endl;
*/

/*
	Fish newFish;
	Animal * aFish;
	Animal * bFish;
	aFish = new Fish{nameA, temper, play, care, age, fresh, hunger, tank, sickness, temp};	
	bFish = new Fish{nameB, temper, play, care, age, fresh, hunger, tank, sickness, temp};	
	cout << "aFish (Marley) is ";
	if(*aFish < *bFish)
		cout << '<';
	else
		cout << "!<";
	cout << " than bFish (Frank)." << endl;
	cout << *aFish;
	aFish->checkDisposition();
	cout <<endl;
	aFish->checkHealth();
	cout <<endl;
	aFish->checkCare();
	cout <<endl;
	aFish->addTrait("Red, blue, and green scales");
	aFish->addTrait("Big buggy eyes");
	aFish->addTrait("Petite scales");

	if(aFish->feedFlakes())
		cout << "Success" << endl;
	else
		cout << "Failure" << endl;
	aFish->giveMedicine();
	cout <<endl;
	int num = 31;
	aFish->play(num);
	cout <<endl;
	aFish->cleanTank();
	cout <<endl;
	aFish->rest();
	cout <<endl;
	aFish->checkDisposition();
	cout <<endl;
	num = -4;
	aFish->setTankCleanliness(num);
	cout << *aFish << endl;

	RBT tree;
	tree.insert(aFish);
	tree.insert(bFish);
	tree.display();
*/


/*
	// constructors
	Fish(const std::string & name, const bool & temper, const int & play, const int & care, const int & age, const bool & fresh, const bool & hunger, const int & tank, const int & disease, const int & temp);
	
	void checkDisposition() const;		// Check temperament only
	void checkHealth() const;		// Check tank cleanliness and diseases
	void checkCare() const;			// Check if freshwater and prefered temp
	bool feedFlakes();			// Check if hungry, feed if true and set hungry to false. Increment tank cleanlines
	bool giveMedicine();			// Check if healthy, if not give medicine and set disease to healthy
	int play(const int & hours);		// Will print a random message about fish
	void cleanTank();			// Set tank cleanliness to 0
	void rest();				// Set hungry to true, increment tank cleanliness

	// setters
	void setFreshwater(const bool & fresh);
	void setHungry(const bool & food);
	void setTankCleanliness(const int & num);
	void setDiagnosis(const int & num);
	void setPreferredTemp(const int & num);

protected:
	//////////////////////// global shared consts ////////////////////////
	enum disease { healthy = 1, dropsy = 2, cotton_mouth = 3, fin_rot = 4 };
	const std::vector<std::string> interactions{ 	" is hanging out in the aquatic plants!",
							" is burrowing under the sand.",
							" is swimming around with the other fish.",
							" follows your finger around.",
							" completely ignores you.",
							" let's their scales sparkle in the light.",
							" glares at you..."				};

	const std::vector<std::pair<int, int>> temperatures{	{50, 60},
								{60,70},
								{70, 80},
								{80, 85}	};


	/////////////////////// Protected Member ////////////////////////////
	const int MAX_FILTH = 10;
	bool freshwater;
	bool hungry;
	int tankCleanliness;
	disease diagnosis;
	int preferredTemp;
*/

	return 0;
}

