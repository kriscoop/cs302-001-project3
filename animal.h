/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: animal.h
Description: 	Here you'll find the header file for the animal heirarchy, including 
		the base class animal and its derived classes Cat, Dog, and Fish.
		I have void functions but will implement exception handling for extra practice



*/

#ifndef ANIMAL_H
#define ANIMAL_H


#include <string>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <utility>
#include <list>
#include <vector>

const int MAX_STRING = 256;

class Animal
{
public:
	//////////////////////// global shared consts ////////////////////////
	const int MAX_CARE = 10;
	const int MAX_PLAYTIME_DURATION = 6;
	const int MAX_PLAYTIME_TOTAL = 18;

	Animal();
	Animal(std::istream & input);
	Animal(	const std::string & name, const bool & temper, const int & play, const int & care, const int & age);
	virtual ~Animal();

	virtual void checkDisposition() const {};	// Does animal play well with others? (temperament)
	virtual void checkHealth() const {};		// Specific to class.
	virtual void checkCare() const {};		// Check how much attentive care required. (low, medium, high);
	virtual int play(const int & hours);		// Increment playtime + specific to class.
	virtual void rest() = 0;			// Set playtime to 0;
	
	// getter
	std::string getName() const;
	virtual std::string typeToString() const;

	// setters
	void setTemperament(const bool & temp);
	void setName(const std::string & name);
	virtual void setPlayTime(const int & play);
	void setCareLevel(const int & care);
	void setAgeGroup(const int & age);
	void addTrait(const std::string & trait); 
	int removeTrait(const int & index);

	// Operator Overloads

	friend bool operator < (const Animal & op1, const Animal & op2);
	friend int operator < (const Animal & op1, const std::string & op2);
	friend std::ostream & operator << (std::ostream & output, const Animal & op1);

protected:
	//////////////////////// global shared consts ////////////////////////
	enum ageGroup {young = 1, adolescent = 2, adult = 3, elder = 4};

	/////////////////////// Protected Member ////////////////////////////
	std::string name;
	std::list<std::string> physicalTraits;
	bool temperament;
	int playTime;
	int careLevel;
	ageGroup age;

	////////////////////// protected Helper functions ///////////////////
	virtual void print(std::ostream & output) const {};
	virtual std::istream & load(std::istream & input);
	std::string careLevelToString(const int & care) const;

};


///////////// CAT HEADER //////////////////////////////
class Cat: public Animal
{
public:
	//////////////////////// global shared consts ////////////////////////
	const int MIN_PLAYTIME = 3;

	Cat();
	Cat(std::istream & input);
	Cat(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const int & weight, const bool & ouch, const int & max, const int & size);
	~Cat();
	
	void checkDisposition() const;			// Check temperament and scratch 
	void checkHealth() const;			// Compare weight to healthy range per age group and size multiplier
	void checkCare() const;				// Check carelevel and max play required 
	int play(const int & hours);			// Will refuse playtime if above Max, will scratch if true
	void rest();					// Will refuse to rest if in bad mood.
	
	// Cat specific
	int giveTreat();				// Set bad mood to false
	
	// getter
	virtual std::string typeToString() const;

	// setters
	void setPlayTime(const int & play);
	void setWeight(const int & weight);
	void setScratch(const bool & ouch);
	void setMaxPlay(const int & play);
	void setSize(const int & size);
	
protected:
	//////////////////////// global shared consts ////////////////////////
	enum sizeClass {small = 1, medium = 2, large = 3};
	// Healthy weights per 4 ageClasses
	const std::vector<std::vector<std::pair<int, int>>> healthyWeightRange{	{{2,3}, {2,4}, {3,4}}, 
										{{4,5}, {4,9}, {5,12}}, 
										{{6,12}, {7, 15}, {8, 20}}, 
										{{6, 10}, {7, 13}, {8, 17}}	};

	/////////////////////// Protected Member ////////////////////////////
	int weight;
	bool scratch;
	bool badMood;
	int maxPlay;
	sizeClass size;

	////////////////////// protected Helper functions ///////////////////
	void reduceWeight(const int reduce);
	void print(std::ostream & output) const;
	std::istream & load(std::istream & input);
};


///////////// DOG HEADER //////////////////////////////
class Dog: public Animal
{
	

public:
	//////////////////////// global shared consts ////////////////////////
	const int MAX_ENERGY = 10;

	Dog();
	Dog(std::istream & input);
	Dog(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const bool & good, const bool & flea, const bool potty, const int & energy);
	~Dog();

	void checkDisposition() const;			// Check temperament and if good dog
	void checkHealth() const;			// Check for fleas and if need bathroom
	void checkCare() const;				// Check energy level and notes
	int play(const int & hours);			// increase energy by hours / 2
	void rest();					// Set bathroom to true
	
	// Dog specific
	bool giveTreatment();				// Set fleas to false;
	int walkDog();				// Set energy to 0 and bathroom to false

	// getter
	virtual std::string typeToString() const;

	// setters
	void setGoodDog(const bool & good);
	void setFleas(const bool & flea);
	void setEnergy(const int & E);
	void addNotes(const std::string & notes);
	void removeNotes(const int & index);

protected:
	/////////////////////// Protected Member ////////////////////////////
	bool goodDog;
	bool fleas;
	bool bathroom;
	int energy;
	std::list<std::string> specialNotes;

	////////////////////// protected Helper functions ///////////////////
	void increaseEnergy(const int num);
	bool catchFleas();
	void print(std::ostream & output) const;
	std::istream & load(std::istream & input);
};


///////////// FISH HEADER //////////////////////////////
class Fish: public Animal
{
public:
	Fish();
	Fish(std::istream & input);
	Fish(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const bool & fresh, const bool & hunger, const int & tank, 
		const int & disease, const int & temp);
	~Fish();
	
	void checkDisposition() const;		// Check temperament only
	void checkHealth() const;		// Check tank cleanliness and diseases
	void checkCare() const;			// Check if freshwater and prefered temp
	int play(const int & hours);		// Will print a random message about fish
	void rest();				// Set hungry to true, increment tank cleanliness

	// Fish specific
	bool feedFlakes();			// Check if hungry, feed if true and set hungry to false. 
						// Increment tank cleanlines
	bool giveMedicine();			// Check if healthy, if not give medicine and set disease 
						// to healthy
	void cleanTank();			// Set tank cleanliness to 0

	// getter
	virtual std::string typeToString() const;

	// setters
	void setFreshwater(const bool & fresh);
	void setHungry(const bool & food);
	void setTankCleanliness(const int & num);
	void setDiagnosis(const int & num);
	void setPreferredTemp(const int & num);

protected:
	//////////////////////// global shared consts ////////////////////////
	enum disease { healthy = 1, dropsy = 2, cotton_mouth = 3, fin_rot = 4 };
	const std::vector<std::string> interactions{ 	" is hanging out in the aquatic plants!",
							" is burrowing under the sand.",
							" is swimming around with the other fish.",
							" follows your finger around.",
							" completely ignores you.",
							" let's their scales sparkle in the light.",
							" glares at you..."				};

	const std::vector<std::pair<int, int>> temperatures{	{50, 60},
								{60,70},
								{70, 80},
								{80, 85}	};


	/////////////////////// Protected Member ////////////////////////////
	const int MAX_FILTH = 10;

	bool freshwater;
	bool hungry;
	int tankCleanliness;
	disease diagnosis;
	int preferredTemp;

	////////////////////// protected Helper functions ///////////////////
	void print(std::ostream & output) const;
	void deIncrementClean();
	void catchDisease(disease & sick);
	std::string diseaseToString(const disease & sick) const;
	std::string temperatureToString(const int & num) const;
	std::istream & load(std::istream & input);
};

#endif


