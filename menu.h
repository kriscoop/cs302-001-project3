/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: menu.h
Description: 	This implements user menu. 
		Only public client interactions are load and run. 



*/

#ifndef MENU_H
#define MENU_H

#include "RBT.h"
#include "CMD.h"


class Menu
{
public:
	Menu();
	Menu(std::istream & input);
	Menu(const RBT & tree);

	int run();
	int loadTree(std::istream & input);

private:
	RBT tree;

	//////////// top menu ///////////
	void printTopMenu() const;
	void topMenuSelect(char & option);
	void topMenuRead(char & option);

	//////////// Create Heirarchy objects ///////
	void createCat(Animal * & temp, char &);
	void createDog(Animal * & temp, char &);
	void createFish(Animal * & temp, char &);

	//////////// Retrieve
	void retrievalProcess(char & option);
	void printRetrieveMenu(Animal * & animal) const;
	void retrieveMenuSelect(Animal * & animal, char & option);
	void retrieveMenuRead(Animal * & animal, char & option);
	void specialtyOptionRead(Animal * & animal, char & option);


	//////////// Cat Manipulation ////////////
	void printCatMenu() const;
	void catSpecialtyRead(Cat * animal, char & option);

	//////////// Dog Manipulation ////////////
	void printDogMenu() const;
	void dogSpecialtyRead(Dog * animal, char & option);

	//////////// Fish Manipulation ////////////
	void printFishMenu() const;
	void fishSpecialtyRead(Fish * animal, char & option);

	//////////// Insert
	void printInsertMenu() const;
	void insertMenuSelect(char & option);
	void insertMenuRead(char & option);

	//////////// Remove All
	void removeAllMenu(char & option);

	void loadAnimalsMenu();

};

#endif
