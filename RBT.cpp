/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: RBT.cpp
Description: 	Here is the implementation for the red black tree and node classes. 



*/

#include "RBT.h"

/////////////////// Node Implementations ///////////////////////

Node :: Node(): left(nullptr), right(nullptr), parent(nullptr), key(nullptr), color(true)
{
}

Node :: Node(Animal * & key): left(nullptr), right(nullptr), parent(nullptr), key(nullptr), color(true)
{
	this->key.reset(key);
	key = nullptr;
}

Node :: Node(const Animal * & key): left(nullptr), right(nullptr), parent(nullptr), key(nullptr), color(true)
{
	copyKey(key);
}

//Copy constructor
Node :: Node(const Node & node): left(nullptr), right(nullptr), parent(nullptr), key(nullptr), color(true)
{
	*this = node;
}

void Node :: copyKey(const Animal * & key)
{
	if(key)
	{
		const Cat * cat = dynamic_cast<const Cat *>(key);
		const Dog * dog = dynamic_cast<const Dog *>(key);
		const Fish * fish = dynamic_cast<const Fish *>(key);
		if(cat)
		{
			Cat * newCat = new Cat{*cat};
			this->key.reset(newCat);
		}
		else if(dog)
		{
			Dog * newDog = new Dog{*dog};	
			this->key.reset(newDog);
		}
		else if(fish)
		{
			Fish * newFish = new Fish{*fish};
			this->key.reset(newFish);
		}
	}
}

//////////// Operator overloads for Node and RBT
Node & Node :: operator=(const Node & node)
{
	if(this != &node)
	{
		if(key)
			key.reset(nullptr);

		if(node.key)
		{
			const Animal * newKey = node.key.get();
			copyKey(newKey);
		}
		color = node.color;
	}
	return *this;
}

RBT & RBT :: operator=(const RBT & tree)
{
	if(root)
		root.reset(nullptr);
		
	try
	{
		if(!tree.root.get())
			throw std::runtime_error("Cannot copy empty tree");

		const Animal * newKey = tree.root->key.get();
		root = std::make_unique<Node>(newKey);

		if(tree.root->left)
			root->left.reset(copyTree(tree.root->left, root.get())); 
		if(tree.root->right)
			root->right.reset(copyTree(tree.root->right, root.get())); 
		return *this;
	}
	catch(const std::exception & ex)
	{
		std::cout << ex.what() << std::endl;
		return *this;
	}
}


std::ostream & operator << (std::ostream & output, const Node & node)
{
	output << *node.key.get();
	return output;
}

std::ostream & operator << (std::ostream & output, const RBT & tree)
{
	static int count = 0;
	tree.displayTreeHelper(output, tree.root.get(), count);
	count = 0;
	return output;
}

/////////////////////// RED BLACK TREE Implementation ///////////////////////

RBT :: RBT(): root(nullptr), current(nullptr)
{
}

RBT :: RBT(std::istream & input): root(nullptr), current(nullptr)
{
	load(input);
}

RBT :: RBT(const Animal * & key): root(nullptr), current(nullptr)
{
	root = std::make_unique<Node>(key);
}

RBT :: RBT(const RBT & tree): root(nullptr), current(nullptr)			// Copy constructor
{
	if(tree.root)
	{
		const Animal * newKey = tree.root->key.get();
		root = std::make_unique<Node>(newKey);

		if(tree.root->left)
			root->left.reset(copyTree(tree.root->left, root.get())); 
		if(tree.root->right)
			root->right.reset(copyTree(tree.root->right, root.get())); 
	}
}

void RBT :: insert(Animal * & newKey) 
{
	if (newKey)
	{
		auto newNode = std::make_unique<Node>(newKey);

		// Set the raw pointer to null after insertion
		newKey = nullptr;
		
		if(!root)
		{
			root = std::move(newNode);
			// Additional steps to fix the red-black tree properties go here
			fixViolation(root);
		}
		else
			insertNode(root, newNode);
	}
}

void RBT :: insertNode(std::unique_ptr<Node> & parent, std::unique_ptr<Node> & newNode)
{
        if (*newNode->key < *parent->key)
	{
		if (parent->left) 
			insertNode(parent->left, newNode);		// Recursive call to end
		else
		{
			newNode->parent = parent.get();			// Attatch to end
			parent->left = std::move(newNode);

			// Additional steps to fix the red-black tree properties go here
			fixViolation(parent->left);
		}
        }
	else
	{
		if (parent->right)
			insertNode(parent->right, newNode);		// Recursive call to end
            	else
		{
			newNode->parent = parent.get();			// Attach to end
			parent->right = std::move(newNode);

			// Additional steps to fix the red-black tree properties go here
			fixViolation(parent->right);
		}
        }
}

/*
// Works, now make recursive
// Balance RBT algorithm
void RBT :: fixViolation(std::unique_ptr<Node> & node) 
{
	auto parent = node->parent;

        while (parent != root.get() && parent && parent->color == true)
	{
		auto grandpa = parent->parent;

		if(grandpa->left.get() == parent)
		{
			auto uncle = grandpa->right.get();

			// Case 1
			if(uncle && uncle->color == true)
			{
				parent->color = false;
				uncle->color = false;
				grandpa->color = true;
				parent = grandpa;
			}
			else
			{
				// Case 2
				if(node == parent->right)
				{
					rotateLeft(std::move(grandpa->left));
					parent = grandpa->left.get();
				}
				parent->color = false;
				parent->parent->color = true;
				auto greatGrandpa = grandpa->parent;

				if(!greatGrandpa)
					rotateRight(std::move(root));

				else if(grandpa == greatGrandpa->left.get())
					rotateRight(std::move(greatGrandpa->left));

				else
					rotateRight(std::move(greatGrandpa->right));
			}
		}
		else
		{
			auto uncle = grandpa->left.get();

			if(uncle && uncle->color == true)
			{
				parent->color = false;
				uncle->color = false;
				grandpa->color = true;
				parent = grandpa;
			}
			else
			{
				if(node == parent->left)
				{
					rotateRight(std::move(grandpa->right));
					parent = grandpa->right.get();
				}
				parent->color = false;
				grandpa->color = true;
				auto greatGrandpa = grandpa->parent;

				if(!greatGrandpa)
					rotateLeft(std::move(root));

				else if(grandpa == greatGrandpa->left.get())
					rotateLeft(std::move(greatGrandpa->left));

				else
					rotateLeft(std::move(greatGrandpa->right));
			}
		}
	}
	root->color = false;
}
*/

// Recursive helper function for fixing nodes 
void RBT :: violationHelper(std::unique_ptr<Node> & node, Node * & parent)
{
        if(parent != root.get() && parent && parent->color == true)
	{
		auto grandpa = parent->parent;

		if(grandpa->left.get() == parent)
		{
		// Parent is left child

			auto uncle = grandpa->right.get();

			// Case 1: Uncle and parent are red
			if(uncle && uncle->color == true)
			{
				parent->color = false;
				uncle->color = false;
				grandpa->color = true;
				parent = grandpa;
				grandpa = parent->parent;
			}
			else
			{
				// Case 2: Uncle is black, parent is red, and node is the inner child of parent 
				if(node == parent->right)
				{
					rotateLeft(std::move(grandpa->left));
					parent = grandpa->left.get();
					grandpa = parent->parent;
				}

				// Recolor parent and grandpa.
				parent->color = false;
				parent->parent->color = true;
				auto greatGrandpa = grandpa->parent;

				// Check for great grandpa and rotate right accordingly
				if(!greatGrandpa)
					rotateRight(std::move(root));

				else if(grandpa == greatGrandpa->left.get())
					rotateRight(std::move(greatGrandpa->left));

				else
					rotateRight(std::move(greatGrandpa->right));
			}
			
		}
		else
		{
		// Parent is right child

			auto uncle = grandpa->left.get();

			// Case 1: Uncle and parent are red
			if(uncle && uncle->color == true)
			{
				parent->color = false;
				uncle->color = false;
				grandpa->color = true;
				parent = grandpa;
				grandpa = parent->parent;
			}
			else
			{
				// Case 2: Uncle is black, parent is red, and node is the inner child of parent 
				if(node == parent->left)
				{
					rotateRight(std::move(grandpa->right));
					parent = grandpa->right.get();
					grandpa = parent->parent;
				}

				// Recolor parent and grandpa.
				parent->color = false;
				grandpa->color = true;
				auto greatGrandpa = grandpa->parent;

				// Check for great grandpa and rotate left accordingly
				if(!greatGrandpa)
					rotateLeft(std::move(root));

				else if(grandpa == greatGrandpa->left.get())
					rotateLeft(std::move(greatGrandpa->left));

				else
					rotateLeft(std::move(greatGrandpa->right));
			}
		}
		// Recursive call. Need actual child unique_ptr<Node> passed by reference so checks required. 
		// Null gaurd
		if(parent)
		{
			auto grandpa = parent->parent;
			
			// if no grandpa, then root.
			if(grandpa)
			{
				// check if grandpa left or right
				if(grandpa->left.get() == parent)
					violationHelper(grandpa->left, grandpa);
				else
					violationHelper(grandpa->right, grandpa);
			}
			else
				violationHelper(root, root->parent);
		}
	}
}


// Recursive Balance RBT algorithm
void RBT :: fixViolation(std::unique_ptr<Node> & node) 
{
	auto parent = node->parent;

	violationHelper(node, parent);

	root->color = false;
}


// Rotating to the left
void RBT :: rotateLeft(std::unique_ptr<Node> && x)
{
	// Switch x's right, y, with y's left
	auto y = std::move(x->right);
	x->right = std::move(y->left);				

	//if not null, assign its parent as x
	if (x->right)
		x->right->parent = x.get();

	//swap y's parent with x's parent
	y->parent = x->parent;				
	auto parent = x->parent;

	// if parent is null, then rotating at root (x at root)
	if (!parent)				
	{
		// Release x at root and assign y to root
		auto temp = x.release();
		root = std::move(y);
		// x is now the left of y, assign x's parent to y
		root->left = std::unique_ptr<Node>(temp);
		root->left->parent = root.get();
	}
	// x is left child of parent 
	else if (x == parent->left) 
	{
		// Release x from parent left and assign y to parent left
		auto temp = x.release();
		parent->left = std::move(y);
		// x is now the left y, assign x's parent to y
		parent->left->left = std::unique_ptr<Node>(temp);
		parent->left->left->parent = parent->left.get();
	}
	// x is right child of parent 
	else
	{
		// Release x from parent right and assign y to parent right 
		auto temp = x.release();
		parent->right = std::move(y);
		// x is now the left of y, assign x's parent to y
		parent->right->left = std::unique_ptr<Node>(temp);
		parent->right->left->parent = parent->right.get();
	}
}

// Rotate to the right 
void RBT :: rotateRight(std::unique_ptr<Node> && x) 
{
	// Switch x's left, y, with y's left 
	auto y = std::move(x->left); 	
	x->left = std::move(y->right); 		

	//if not null, assign it's parent as x
	if (x->left)
		x->left->parent = x.get();

	//swap y's parent with x's parent
	y->parent = x->parent; 					
	auto parent = x->parent;

	// if parent is null, then rotating at root (x at root)
	if (!parent)					
	{
		// Release x at root and assign y to root
		auto temp = x.release();
		root = std::move(y);
		// x is now the right of y, assign x's parent to y
		root->right = std::unique_ptr<Node>(temp);
		root->right->parent = root.get();
	}
	// x is right child of parent 
	else if (x == parent->right) 
	{
		// Release x from parent right and assign y to parent right 
		auto temp = x.release();
		parent->right = std::move(y);
		// x is now the right of y, assign x's parent to y
		parent->right->right = std::unique_ptr<Node>(temp);
		parent->right->right->parent = parent->right.get();
	}
	// x is left child of parent 
	else
	{
		// Release x from parent left and assign y to parent right 
		auto temp = x.release();
		parent->left = std::move(y);
		// x is now the right y, assign x's parent to y
		parent->left->right = std::unique_ptr<Node>(temp);
		parent->left->right->parent = parent->left.get();
	}

}

// Sets current to the retrieved key if applicable
Animal * RBT :: retrieve(const std::string & key) 
{
	Animal * result = nullptr;
	current = retrieveHelper(root.get(), key);
	if(current)
		result = current->key.get();
	return result;
}

Node * RBT :: retrieveHelper(Node* node, const std::string & key) const 
{
        if (node)
	{
		int test = *node->key < key;

		if (test < 0)
			return retrieveHelper(node->right.get(), key);		// Key might be in the left subtree
		else if (test > 0)
			return retrieveHelper(node->left.get(), key);		// Key might be in the right subtree
		else
			return node;						// Key found
        }
	return nullptr;								// Key not found
}

// destroy tree's nodes
void RBT :: removeAll()
{
	root.reset(nullptr);
	if(current)
    		current = nullptr;
}

// Display node information
void RBT :: display() const
{
	static int count = 0;
	displayHelper(root.get(), count);
	count = 0;
}

// Load tree from input file 
int RBT :: loadTree(std::istream & input)
{
	if(input)
	{
		load(input);
		return 1;
	}
	return 0;
}

// Display node information
void RBT :: displayHelper(const Node* node, int & count) const 
{
	if (node) 
	{
		displayHelper(node->left.get(), count);
		++count;
		std::cout << "/---------------------- Node " << count << " ------------------------/\n";
		std::cout << "\n" << *node << "\n\n";
		displayHelper(node->right.get(), count);
	}
}

// Display node structure
void RBT :: displayTreeHelper(std::ostream & output, const Node * node, int & count) const 
{
	if(node)
	{
		displayTreeHelper(output, node->left.get(), count);
		Node * parent = node->parent;
		++count;
		output << count;
		if(parent)
		{
			output << ".\t\tParent: " << parent->key->getName() << " -> ";
			if(parent->left.get() == node)
				output <<  "Left\n";
			else
				output <<  "Right\n";
		}
		else
			output << ".\t\tROOT!\n";
		output << "Name: " << node->key->getName() << std::endl;	
		output << "Color: ";
		if(node->color == true)
			output << "Red" << std::endl;
		else
			output << "Black" << std::endl;
		output << "Type: " << node->key->typeToString() << "\n\n";
		displayTreeHelper(output, node->right.get(), count);
	}

}

// copy recursive helper
Node * RBT :: copyTree(const std::unique_ptr<Node> & node, Node * parent)
{
	Node * aNode = nullptr;

	if(node)
	{
		const Animal * key = node->key.get();
		aNode = new Node(key);
		aNode->color = node->color;
		aNode->parent = parent;

		if(node->left)
			aNode->left.reset(copyTree(node->left, aNode));
		if(node->right)
			aNode->right.reset(copyTree(node->right, aNode));
	}	
		return aNode;
}

// Load from file helper
std::istream & RBT :: load(std::istream & input)
{
	if(input)
	{
		int type;
		input >> type;
		try
		{	
			if(!input.eof())
			{
				Animal * animal;

				while(!input.eof())
				{
					input.ignore(1, ';');
					if(type == 1)
						animal = new Cat(input);
					else if(type == 2)
						animal = new Dog(input);
					else if(type == 3)
						animal = new Fish(input);
					else
						throw std::runtime_error("Error: Incorrectly formatted data file.");
					insert(animal);
	
					input >> type;
				}
			}
		}
		catch(const std::exception & ex)
		{
			std::cout << ex.what() << std::endl;
		}
	}
	return input;
}

