/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: CMD.h
Description: 	This file contains a collection of helper functions for user prompts.



*/

#ifndef CMD_H
#define CMD_H


#include <string>
#include <cctype>
#include <iostream>
#include "animal.h"


bool validInput();
int getInt();
int getInt(const int, const int);
long getLong();
long getLong(const long, const long);
float getFloat();
float getFloat(const float, const float);
double getDouble();
double getDouble(const double, const double);
std::string getText();
void getText(char buffer[]);
char getChar();
bool yesNo(char& option);



#endif
