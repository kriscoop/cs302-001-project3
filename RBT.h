/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: RBT.h
Description:	Here is the implementation for the red black tree and node class.
		I wanted to define the node class inside of the RBT but couldn't figure out the
		how to make friend functions work for a nested class. 	



*/

#ifndef RBT_H
#define RBT_H

#include <memory>
#include "animal.h"

//-----------------------------------------------------------------//
	/////////////// Node Class Definition //////////////
class Node
{
public:
	Node();
	Node(Animal * & key);
	Node(const Animal * & key);
	Node(const Node & node);

	Node & operator=(const Node & node);
	friend std::ostream & operator << (std::ostream & output, const Node & node);
	friend class RBT;

private:
	std::unique_ptr<Node> left;
	std::unique_ptr<Node> right;
	Node * parent;
	std::unique_ptr<Animal> key;
	bool color;					// true for Red, false for Black

	void copyKey(const Animal * & key);
};
/////////////// End Node Class Definition ///////////
//-----------------------------------------------------------------//


class RBT
{
public:
	RBT();
	RBT(std::istream & load);
	RBT(const Animal * & key);
	RBT(const RBT & tree);			// Copy constructor

	void insert(Animal * & key);
	Animal * retrieve(const std::string & key);
	void removeAll();
	void display() const;
	int loadTree(std::istream & input);

	RBT & operator=(const RBT & tree);
	friend std::ostream & operator << (std::ostream & output, const RBT & tree);

private:

	/////////////// RBT Privte Data Members /////////////
	std::unique_ptr<Node> root;
	Node * current; 					// Pointer to the last retrieved node
	

	/////////////// RBT Privte Helper Functions /////////
	void insertNode(std::unique_ptr<Node> & parent, std::unique_ptr<Node> & newNode);
	void fixViolation(std::unique_ptr<Node> & node);
	void violationHelper(std::unique_ptr<Node> & noce, Node * & parent);
	void rotateLeft(std::unique_ptr<Node> && x);
	void rotateRight(std::unique_ptr<Node> && x);
	void displayHelper(const Node* node, int & count) const; 
	void displayTreeHelper(std::ostream & output, const Node * node, int & count) const; 
	Node * retrieveHelper(Node* node, const std::string & key) const;
	Node * copyTree(const std::unique_ptr<Node> & node, Node * parent);
	std::istream & load(std::istream & input);

};

#endif

