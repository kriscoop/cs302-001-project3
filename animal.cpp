/*
Name: Kristian Cooper
Class#: CS-302-001 
Project3
File: animal.cpp
Description: 	Here is the implementation for animal class and it's derived classes cat, dog, and fish



*/

#include "animal.h"

//////////////////////////// ANIMAL IMPLEMENTATION ////////////////////////////
	
// constructors
Animal :: Animal(): name("Unknown"), temperament(false), playTime(0), careLevel(0), age(young)
{
	physicalTraits.clear();
}

Animal :: Animal(std::istream & input): name("Unknown"), temperament(false), playTime(0), careLevel(0), age(young)
{
	load(input);
}

Animal :: Animal(	const std::string & aName, const bool & temper, const int & play, const int & care, 
			const int & age): 
			name(aName), temperament(temper), playTime(0), careLevel(5), age(young)
{
	setPlayTime(play);
	setCareLevel(care);
	setAgeGroup(age);
}

Animal :: ~Animal()
{
}

// Should increment playtime as without exceeding max. Exceptionally handled.
int Animal :: play(const int & hours)
{
	int test = playTime + hours;
	std::string buffer = "Error: ";

	try
	{
		if(playTime == MAX_PLAYTIME_TOTAL)
		{
			buffer += name + " has already played the maxmimum amount for today. (";
			buffer += std::to_string(MAX_PLAYTIME_TOTAL) + "hrs)"; 
			throw std::runtime_error(buffer);
		}
		else if(hours < 1 || hours > MAX_PLAYTIME_DURATION)
		{
			buffer += "Can only play a number of hours between range [1, ";
			buffer += std::to_string(MAX_PLAYTIME_DURATION) + "]."; 
			throw std::runtime_error(buffer);
		}
		else if(test > MAX_PLAYTIME_TOTAL)
		{
			int reducedHours = hours - (test - playTime);
			playTime += reducedHours;

			buffer += "You are only able to play with " + name  + " " + std::to_string(reducedHours);
			buffer += "hrs before reaching the maximum allowed play time per day. (";
			buffer +=  std::to_string(MAX_PLAYTIME_TOTAL) + "hrs)";

			std::cout << buffer << std::endl;
			return reducedHours;
		}
		else
			std::cout << "You play with " << name << " for " << hours << "hrs." << std::endl;
		playTime = test;
		return hours;
	}
	catch(const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
		return 0;
	}
}

//getter
std::string Animal :: getName() const
{
	return name;
}
	
std::string Animal :: typeToString() const
{
	return "Animal (Error, ABC)";
}

// setters
void Animal :: setTemperament(const bool & temp)
{
	temperament = temp;
}

void Animal :: setName(const std::string & name)
{
	this->name = name;
}

// Set the current playtime up to maximum.
void Animal :: setPlayTime(const int & play)
{
	try
	{
		if(play < 0 || play > MAX_PLAYTIME_TOTAL)
			throw play;
		else
			playTime = play;
	}
	catch(int play)
	{
		std::cout << "Current play time must be in range [0, " << MAX_PLAYTIME_TOTAL << "]." << std::endl;
	}
}

// Set the care level up to the maximum. 
void Animal :: setCareLevel(const int & care)
{
	try
	{
		if(care <= 0 || care > MAX_CARE)
			throw care;
		else
			careLevel = care;
	}
	catch(int care)
	{
		std::cout << "Error: Care level must be in range [1, " << MAX_CARE << "]." << std::endl;
	}
}

// Set the enum for agegroup. Exceptionaly handled.
void Animal :: setAgeGroup(const int & age)
{
	try
	{
		if(age <= 0 || age > 4)
			throw age;
		else
			this->age = static_cast<ageGroup>(age);
	}
	catch(int age)
	{
		std::cout << "Error: Age classification must be in range [1, 4]." << std::endl;
	}
}

//Add physical traits
void Animal :: addTrait(const std::string & trait) 
{
	physicalTraits.push_back(trait);
}

// Remove traits by index. Exceptionally handled.
int Animal :: removeTrait(const int & index)
{
	try
	{
		if(physicalTraits.empty())
			throw physicalTraits;

		if(index <= 0 || index >= (int) physicalTraits.size())
			throw index;

		std::list<std::string>::iterator it = physicalTraits.begin();

		for(int i = 0; i<index-1; i++)
			++it;
			
		physicalTraits.erase(it);	
		return 1;
	}
	catch(std::list<std::string> physicalTraits)
	{
		std::cout << "Error: List is empty." << std::endl;
		return 0;
	}
	catch(int index)
	{
		std::cout << "Error: Index " << index << " is out of range [1, " << physicalTraits.size() << "]." << std::endl;
		return -1;
	}
	catch(...)
	{
		std::cout << "Error: Refer to animal.cpp:95" << std::endl;
		return -1;
	}
}

std::istream & Animal :: load(std::istream & input)
{
	if(input)
	{
		std::string buffer;
		getline(input, buffer);
		
		if(!input.eof())
		{
			name = buffer;

			input >> temperament;
			input.ignore(1, ';');

			int play;
			input >> play;
			setPlayTime(play);
			input.ignore(1, ';');
			
			int care;
			input >> care;
			setCareLevel(care);
			input.ignore(1, ';');
			
			int age;
			input >> age;
			setAgeGroup(age);
			input.ignore(1, ';');

			int traits;
			input >> traits;
			input.ignore(1, '\n');
			
			for(int i = 0; i< traits;i++)
			{
				getline(input, buffer);
				addTrait(buffer);
			}
		}
	}
	return input;
}

// Operator overloads for comparison of keys. 
bool operator < (const Animal & op1, const Animal & op2)
{
	int condition = op1.name.compare(op2.name);
	
	if(condition < 0)
		return true;
	else
		return false;
}

int operator < (const Animal & op1, const std::string & op2)
{
	return strcmp(op1.name.c_str(), op2.c_str());
}

// Operator overload to print
std::ostream & operator << (std::ostream & output, const Animal & op1)
{
	op1.print(output);
	return output;
}

// Assist to print care level
std::string Animal :: careLevelToString(const int & care) const
{
	std::string test = "error";
	switch(care)
	{
		case 1 ... 3:
			test = "mild";
			break;
		case 4 ... 5:
			test = "moderate";
			break;
		case 6 ... 8:
			test = "hig/";
			break;
		case 9 ... 10:
			test = "extreme";
			break;
	}

	return test;		
}

//////////////////////////// CAT IMPLEMENTATION ////////////////////////////

Cat :: Cat(): weight(0), scratch(false), badMood(false), maxPlay(Animal::MAX_PLAYTIME_DURATION), size(small)
{
}

Cat :: Cat(std::istream & input): 	Animal(input), weight(0), scratch(false), badMood(false), 
					maxPlay(Animal::MAX_PLAYTIME_DURATION), size(small)
{
	load(input);
}

Cat :: Cat(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const int & aWeight, const bool & ouch, 
		const int & max, const int & aSize): 
		Animal(name, temper, 0, care, age), weight(0), scratch(ouch),badMood(false), 
		maxPlay(0), size(small)
{
	setSize(aSize);
	setMaxPlay(max);
	setPlayTime(play);
	setWeight(aWeight);
}

Cat :: ~Cat()
{
}
	
void Cat :: checkDisposition() const			// Check temperament and scratch 
{
	if(temperament)
		std::cout << "This cat plays well with others. ";
	else
		std::cout << "Caution. This cat does not play well with others. ";
	if(scratch)
		std::cout << "Be cautious, they tend to scratch!" ;
	else
		std::cout << "They tend not to scratch." ;
	std::cout << std::endl;
}

void Cat :: checkHealth() const			// Compare weight to healthy range per age group and size multiplier
{
	int min = healthyWeightRange.at((int) age - 1).at((int) size - 1).first;
	int max = healthyWeightRange.at((int) age - 1).at((int) size - 1).second;

	std::cout << name << " weighs " << weight << "lbs. Healthy weight ranges for " << age << " cats go from "; 
	std::cout << min << " - " << max << '.';

	if(weight < healthyWeightRange.at((int) age - 1).at((int) size - 1).first) 
		std::cout << "They are underweight.";
	else if(weight > healthyWeightRange.at((int) age - 1).at((int) size - 1).second)
		std::cout << "They are overweight.";
	else
		std::cout << "They are a healthy weight!";

	std::cout << std::endl;
}

void Cat :: checkCare() const				// Check carelevel and max play required 
{
	int min = healthyWeightRange.at((int) age - 1).at((int) size - 1).first;
	int max = healthyWeightRange.at((int) age - 1).at((int) size - 1).second;

	std::cout << name << " requires a " << careLevelToString(careLevel) << " amount of care." << std::endl;

	if(weight < min)
		std::cout << "They are underweight. We recommend to give them more treats and less play time.";
	else if(weight < max)
		std::cout << "They are overweight. We recommend to give them more play time and less treats.";
	else
		std::cout << "They are a healthy weight!";

	std::cout << std::endl << name << " prefers to play no more than " << maxPlay << " hrs. ";
	std::cout << "They have played a total of " << playTime << " hrs today." << std::endl;
}

int Cat :: play(const int & hours)			// Will refuse playtime if above Max, will scratch if true
{
	if(!badMood)
	{
		int test = playTime + hours;
		std::string buffer = "Error: ";

		try
		{
			if(playTime == maxPlay)
			{
				buffer += name + " has already played the maxmimum amount for today. (" + std::to_string(playTime) + " hrs)";
				if(scratch)
					buffer += " Ouch! You've been scratched.";
				throw std::runtime_error(buffer);
			}
			else if(hours <= 0)
			{
				buffer += "Error: Number of hours to play cannot be less than or equal to 0.";
				throw std::runtime_error(buffer);
			}
			else if(test > maxPlay)
			{
				int reducedHours = hours - (test - maxPlay);
				reduceWeight(reducedHours/2);
		 
				std::cout << "Error: " << name << " prefers to play no more than " << maxPlay << " hrs and they've already had" << playTime;
				std::cout << " hrs of play time." << std::endl;
				std::cout << "You are able to play with them " << reducedHours << " hrs before putting them in a bad mood.";
				std::cout << std::endl;
				playTime += reducedHours;
				badMood = true;
				return -1;
			}
			else
			{
				reduceWeight(hours/2);
				playTime = test;
				std::cout << "You spend " << hours << " hrs playing with " << name << '.' << std::endl;
				return 1;
			}
		}
		catch(const std::exception &ex)
		{
			std::cout << ex.what() << std::endl;
			return 0;
		}
	}
	std::cout << name << " is in a bad mood. They refuse to play with you. ";
	if(scratch)
		std::cout << "They scratch you! ";
	std::cout << "Try giving them a treat to cheer them up." << std::endl;
	return 0;
}

// getter
std::string Cat :: typeToString() const
{
	return "Cat";
}

// Cat specific method	
int Cat :: giveTreat()				// Set bad mood to false and increment weight
{
	int test = -1;

	std::cout << "You give " << name << " a treat. They purr with happiness.\n";

	if(badMood == false)
		test = 1;

	badMood = false;
	++weight;

	return test;
}

void Cat :: rest()					// Will refuse to rest if in bad mood.
{
	if(badMood)
	{
		std::cout << name << " will not go to bed until you give them a treat. ";
		if(scratch)
			std::cout << "Ouch! You've been scratched.";
		std::cout << std::endl;
	}
	else
		std::cout << name << " gets a good nights rest." << std::endl;
	playTime = 0;
}
	
// Overloaded setter 
void Cat :: setPlayTime(const int & play)
{
	try
	{
		if(play < 0 || play > maxPlay)
		{
			if(play < 0)
				playTime = 0;
			else
				playTime = maxPlay;
			throw play;
		}
		else
			playTime = play;
	}
	catch(int play)
	{
		std::cout << "Current play time must be in range [0, " << maxPlay << "].\n";
		std::cout << "Current play time has been set to " << playTime << std::endl;
	}
}

// Specific setters for the cat specific traits
void Cat :: setWeight(const int & weight)
{
	try
	{
		if(weight < 1)
			throw weight;
		else
			this->weight = weight;
	}
	catch(int weight)
	{
		std::cout << "Error: Weight cannot be less than 1." << std::endl;
	}
}

void Cat :: setScratch(const bool & ouch)
{
	scratch = ouch;
}

void Cat :: setMaxPlay(const int & play)
{
	try
	{
		if(play < 3 || play > MAX_PLAYTIME_DURATION )
		{
			if(play < 3)
				maxPlay = 3;
			else 
				maxPlay = MAX_PLAYTIME_DURATION;
			throw play;
		}
		else
			maxPlay = play;
	}
	catch(int play)
	{
		std::cout << "Error: Max play time must be in the range [3, " << MAX_PLAYTIME_TOTAL << "]." << std::endl;
	}
}

void Cat :: setSize(const int & size)
{
	try
	{
		if(size < 1 || size > 3)
			throw size;
		else
			this->size = static_cast<sizeClass>(size);
	}
	catch(int size)
	{
		std::cout << "Error: Size classification must be in range [1, 3]." << std::endl;
	}
}
	
// Assist with incrementing weight
void Cat :: reduceWeight(const int weight)
{
	int test = this->weight - weight;
	std::string buffer = "Error: ";

	try
	{
		if(test <= 1)
		{
			buffer += name + " is dangerously underweight. We recommend giving them more treats."; 
			this->weight = 1;
			throw std::runtime_error(buffer);
		}
		if(weight < 0)
		{
			buffer += "Cannot reduce weight by a negative amount."; 
			throw std::runtime_error(buffer);
		}
		else
			this->weight = test;
	}
	catch(const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
}

// virtual print specific to each class
void Cat :: print(std::ostream & output) const
{
	output << "Name: " << name << "\tAge: " << age << "\nWeight: " << weight << "\tPlaytime: " << playTime; 
	output << "\t Max Playtime: " << maxPlay << "\nCare Level: " << careLevel << " (" << careLevelToString(careLevel) << ")\n";
	output << "Notes:\tThis cat ";
	if(temperament)
		output << "does not";
	else
		output << "does";
	output << " play well with others";
	if(scratch)
		output << " and tends to scratch."; 
	else
		output << '.';
	output << std::endl << "\tThey prefer no more than " << maxPlay << "hrs of play time a day before feeling bothered.";
	output << std::endl << "\tThey are currently in a ";
	if(badMood)
		output << "bad";
	else
		output << "good";
	output << " mood." << std::endl << "Physical Traits: ";
	if(physicalTraits.empty())
		output << std::endl << "\tNothing of note.\n";
	else
	{
		int i = 0;
		for(auto it: physicalTraits)
		{
			++i;
			output << std::endl << '\t' << i <<". " << it;
		}
	}
}

std::istream & Cat :: load(std::istream & input)
{
	if(input)
	{
		int pounds;
		input >> pounds;
		
		if(!input.eof())
		{
			setWeight(pounds);
			input.ignore(1, ';');
			
			int max;
			input >> max;
			setMaxPlay(max);
			input.ignore(1, ';');
			
			setPlayTime(playTime);

			int size;
			input >> size;
			setSize(size);
			input.ignore(1, ';');

			input >> badMood;
			input.ignore(1, ';');

			input >> scratch;
			input.ignore(1, '\n');
		}
	}
	return input;
}

//////////////////////////// DOG IMPLEMENTATION ////////////////////////////

Dog :: Dog(): goodDog(true), fleas(false), bathroom(false), energy(10)
{
}

Dog :: Dog(std::istream & input): Animal(input), goodDog(true), fleas(false), bathroom(false), energy(10)
{
	load(input);
}

Dog :: Dog(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const bool & good, const bool & flea, const bool potty, const int & energy): 
		Animal(name, temper, play, care, age), goodDog(good), fleas(flea), bathroom(potty), energy(0)
{
	setEnergy(energy);
}

Dog :: ~Dog()
{
}

void Dog :: checkDisposition() const			// Check temperament and if good dog
{
	if(temperament)
		std::cout << "This dog plays well with others. ";
	else
		std::cout << "Caution. This dog does not play well with others. ";
	if(goodDog)
		std::cout << "They generally tend to be a good dog."; 
	else
		std::cout << "They generally tend to be a bad dog.";
	std::cout << std::endl;
	
}

void Dog :: checkHealth() const			// Check for fleas and if need bathroom
{
	if(fleas)
		std::cout << "This dog appears to have fleas. We recommend administering a flea treatment." << std::endl;
	else
		std::cout << "This dog appears to be doing fine." << std::endl;
}

// getter
std::string Dog:: typeToString() const
{
	return "Dog";
}

// Dog specific function
bool Dog :: giveTreatment()
{
	if(fleas)
	{
		fleas = false;
		std::cout << "You have administered the flea treatment. " << name << " looks happy.\n";
		return true;
	}
	else
		std::cout << name << " looks perfectly fine. No need to administer the flea treatment.\n";
	std::cout << std::endl;
	return false;
		
}

void Dog :: checkCare() const				// Check energy level and notes
{
	std::cout << "This dogs energy level is currently at a " << energy << '/' << MAX_ENERGY << '.' << std::endl;
	if(energy == MAX_ENERGY)
		std::cout << "You should take them on a walk to get rid of all that energy!" << std::endl;
	if(energy == MAX_ENERGY/3)
		std::cout << "Try playing with them to rile them up a bit." << std::endl;
	if(bathroom)
		std::cout << "They appear to need a bathroom break. You should take them outside." << std::endl;
}

int Dog :: play(const int & hours)			// increase energy by hours / 2
{
	if(energy > 0)
	{
		int hoursPlayed = Animal::play(hours);
		
		if(hoursPlayed > 0)
		increaseEnergy(hoursPlayed/2);

		return hoursPlayed;
	}
	else
		std::cout << name << " looks tuckered out. You should let them rest." << std::endl;
	return 0;
}

// Dog specific function
int Dog :: walkDog()				// Set energy to 0 and bathroom to false
{
	if(energy > 0)
	{
		bathroom = false;
		bool chance = catchFleas();
		energy = 0;
		std::cout << "You have a nice walk with " << name << ". They used the restoom and look ready for a rest.";
		if(chance)
			std::cout << "\nUh oh! It looks like " << name << " may need a flea treatment.";
		std::cout << std::endl;
		return 1;
	}
	else
		std::cout << name << " looks tuckered out. You should let them rest." << std::endl;
	return 0;
}

void Dog :: rest()					// Set bathroom to true
{
	bathroom = true;
	energy = MAX_ENERGY/3;
	std::cout << name << " had a good rest. They wake up looking a little groggy." << std::endl;
}
	
// setters
void Dog :: setGoodDog(const bool & good)
{
	goodDog = good;
}

void Dog :: setFleas(const bool & flea)
{
	fleas = flea;
}

void Dog :: setEnergy(const int & E)
{
	try
	{
		if(E < 0 || E > MAX_ENERGY)
			throw E;
		else
			energy = E;
	}
	catch(int E)
	{
		std::cout << "Error: Energy must be within range [0, " << MAX_ENERGY << "]." << std::endl;
	}
}
// Dog specific method
void Dog :: addNotes(const std::string & notes)
{
	specialNotes.push_back(notes);
}

void Dog :: removeNotes(const int & index)
{
	try
	{
		if(specialNotes.empty())
			throw specialNotes;

		if(index <= 0 || index >= (int) specialNotes.size())
			throw index;
			
		std::list<std::string>::iterator it = specialNotes.begin();

		for(int i = 0; i<index-1; i++)
			++it;
			
		specialNotes.erase(it);	
	}
	catch(std::vector<std::string> specialNotes)
	{
		std::cout << "Error: List is empty." << std::endl;
	}
	catch(int index)
	{
		std::cout << "Error: Index " << index << " is out of range [1, " << physicalTraits.size() << "]." << std::endl;
	}
	catch(...)
	{
		std::cout << "Error: Refer to animal.cpp:559" << std::endl;
	}

}

// Assisting function 
void Dog :: increaseEnergy(const int num)
{
	int test = energy + num;
	std::string buffer = "Error: ";

	try
	{
		if(energy == MAX_ENERGY)
		{
			buffer += name + " is currently at maximum energy. (" + std::to_string(MAX_ENERGY) + ") You should take them for a walk.";
			throw std::runtime_error(buffer);
		}
		else if(num < 0)
		{
			buffer += "Error: Energy level cannot increase by less than 0.";
			throw std::runtime_error(buffer);
		}
		else if(test > MAX_ENERGY)
		{
			buffer += "Energy level cannot exceed max energy level of " + std::to_string(MAX_ENERGY) + ". Currently at " + std::to_string(energy) + '/';
			buffer += std::to_string(MAX_ENERGY) + " attempting to add " + std::to_string(num) + ".";
			throw std::runtime_error(buffer);
		}
		else
			energy = test;
	}
	catch(const std::exception & ex)
	{
		std::cout << ex.what() << std::endl;
	}
}

// Assist function
bool Dog :: catchFleas()
{
	time_t current_time = std::time(NULL);
	std::srand((unsigned) current_time);
	int fleas = (std::rand() % 5);

	if(fleas == 4)
		return true;
	else
		return false;
}

// virtual print function
void Dog :: print(std::ostream & output) const
{
	output << "Name: " << name << "\tAge: " << age << "\nPlaytime: " << playTime << "\tMax Playtime: " << MAX_PLAYTIME_TOTAL;
	output << "\nCare Level: " << careLevel << " (" << careLevelToString(careLevel) << ")\n";
	output << "Notes:\tThis dog ";
	if(temperament)
		output << "does not";
	else
		output << "does";
	output << " play well with others and is generally a ";
	if(goodDog)
		std::cout << "good";
	else
		std::cout << "bad";
	output << " dog. \n\tThey";
	if(!fleas)
		output << " do not";
	output << " need a flea treatment. They";
	if(!bathroom)
		output << " do not";  	
	output << " need to use the bathroom.\nThey are currently at a " << energy << '/' << MAX_ENERGY << " energy level." << std::endl; 
	
	output << "Physical Traits: ";
	if(physicalTraits.empty())
		output << std::endl << "\tNothing of note.\n";
	else
	{
		int i = 0;
		for(auto it: physicalTraits)
		{
			++i;
			output << std::endl << '\t' << i <<". " << it;
		}
	}

	output << "\nSpecial Notes: ";
	if(specialNotes.empty())
		output << std::endl << "\tNothing of note.\n";
	else
	{
		int i = 0;
		for(auto it: specialNotes)
		{
			++i;
			output << std::endl << '\t' << i <<". " << it;
		}
	}
}

std::istream & Dog :: load(std::istream & input)
{
	if(input)
	{
		bool good;
		input >> good;
		
		if(!input.eof())
		{
			goodDog = good;
			input.ignore(1, ';');
			
			input >> fleas;
			input.ignore(1, ';');

			input >> bathroom;
			input.ignore(1, ';');
			
			int E;
			input >> E;
			setEnergy(E);
			input.ignore(1, ';');
			
			std::string buffer;

			int notes;
			input >> notes;
			input.ignore(1, '\n');
			for(int i = 0; i< notes;i++)
			{
				getline(input, buffer);
				addNotes(buffer);
			}
		}
	}
	return input;
}

//////////////////////////// FISH IMPLEMENTATION ////////////////////////////

Fish :: Fish(): freshwater(false), hungry(false), tankCleanliness(0), diagnosis(healthy), preferredTemp(0) 
{
}

Fish :: Fish(std::istream & input): Animal(input), freshwater(false), hungry(false), tankCleanliness(0), diagnosis(healthy), preferredTemp(0) 
{
	load(input);	
}

Fish :: Fish(	const std::string & name, const bool & temper, const int & play, const int & care, 
		const int & age, const bool & fresh, const bool & hunger, const int & tank, const int & disease, 
		const int & temp): 
		Animal(name, temper, play, care, age), freshwater(fresh), hungry(hunger), tankCleanliness(0), 
		diagnosis(healthy), preferredTemp(0)
{
	setTankCleanliness(tank);
	setPreferredTemp(temp);
	setDiagnosis(disease);
}

Fish :: ~Fish()
{
}

void Fish :: checkDisposition() const		// Check temperament only
{
	if(temperament)
		std::cout << "This fish plays well with others. ";
	else
		std::cout << "Caution. This fish does not play well with others. ";
	std::cout << std::endl;
}
	
void Fish :: checkHealth() const		// Check tank cleanliness and diseases
{
	std::cout << "This fish is";
	if(!hungry)
		std::cout << " not";
	std::cout << " hungry. Their tanks cleanliness is a " << tankCleanliness << "/" << MAX_FILTH << "." << std::endl;

	std::cout << name << " looks like a " << diseaseToString(diagnosis) << std::endl;
}

void Fish :: checkCare() const			// Check if freshwater and prefered temp
{
	std::cout << "This is a ";
	if(freshwater)
		std::cout << "freshwater";
	else
		std::cout << "saltwater";
	std::cout << " fish. Their prefered temperature range is " << temperatureToString(preferredTemp) << '.' << std::endl;
}

// getter
std::string Fish :: typeToString() const
{
	return "Fish";
}

// Fish specific function
bool Fish :: feedFlakes()			// Check if hungry, feed if true and set hungry to false. Increment tank cleanlines
{
	deIncrementClean();
	if(hungry)
	{
		hungry = false;
		std::cout << "You fed " << name << ". They look to be enjoying their flakes.";
		return true;
	}
	else
		std::cout << name << " does not look hungry. They don't need flakes at this time.";
	std::cout << std::endl;
	return false;
}

// Fish specific function
bool Fish :: giveMedicine()			// Check if healthy, if not give medicine and set disease to healthy
{
	if(diagnosis != healthy)
	{
		diagnosis = healthy;
		std::cout << "You administer the medicine. " << name << " looks happy and healthy!" << std::endl;
		return true;
	}
	else
		std::cout << name << " looks perfectly fine. No need to administer medicine." << std::endl;
	return false;
}

int Fish :: play(const int & hours)		// Will print a random message about fish
{
	int played = Animal::play(hours);

	if(played != 0)
	{
		time_t current_time = std::time(NULL);
		std::srand((unsigned) current_time);
		int message = (std::rand() % interactions.size());
		
		std::cout << "You peer into the tank. " << name << interactions.at(message) << std::endl;
		std::cout << "The tank is currently at a " << tankCleanliness << '/' << MAX_FILTH << " cleanliness level. " << std::endl;
	}
	return played;
}

// Fish specific function
void Fish :: cleanTank()			// Set tank cleanliness to 10
{
	if(tankCleanliness != 10)
	{
		tankCleanliness = 10;
		std::cout << "You clean the tank. " << name << " looks really happy with their home.";
	}
	else
		std::cout << "The tank is already clean.";
	std::cout << std::endl;
}

void Fish :: rest()				// Set hungry to true, increment tank cleanliness
{
	deIncrementClean();
	std::cout << name << " had a good rest. ";
	if(tankCleanliness <= MAX_FILTH * 1 / 3) 
	{
		std::cout << "You should probably consider taking a rest soon.";
		catchDisease(diagnosis);
	}
	hungry = true;
	std::cout << std::endl;
}

// setters
void Fish :: setFreshwater(const bool & fresh)
{
	freshwater = fresh;
}
 
void Fish :: setHungry(const bool & food)
{
	hungry = food;
}

void Fish :: setTankCleanliness(const int & num)
{
	try
	{
		if(num > MAX_FILTH || num < 0)
			throw num;
		else
			tankCleanliness = num;
	}
	catch(int num)
	{
		std::cout << "Error: Tank cleanliness must be in the range [0, " << MAX_FILTH << "]." << std::endl;
	}
}

void Fish :: setDiagnosis(const int & num)
{
	try
	{
		if(num <= 0 || num > 4)
			throw num;
		else
			diagnosis = static_cast<disease>(num);
	}
	catch(int num)
	{
		std::cout << "Error: Disease diagnosis must be in range [1, 4]." << std::endl;
	}
}

void Fish :: setPreferredTemp(const int & num)
{
	int max = temperatures.size();
	try
	{
		if(num < 1 || num > max)
			throw num;
		else
			preferredTemp = num - 1;
	}
	catch(int num)
	{
		std::cout << "Error: Preferred temperature selection must be in range [1, " << max << "]." << std::endl;
	}
}

// Assist function dependent on tank cleanliness
void Fish :: catchDisease(disease & sick)
{
	time_t current_time = std::time(NULL);
	std::srand((unsigned) current_time);
	int pathogen = (std::rand() % 4);

	setDiagnosis(pathogen);
}


void Fish :: print(std::ostream & output) const
{
	output << "Name: " << name << "\tAge: " << age << "\nPlaytime: " << playTime << "\tMax Playtime: " << MAX_PLAYTIME_TOTAL;
	output << "\nCare Level: " << careLevel << " (" << careLevelToString(careLevel) << ")\n";
	output << "Notes:\tThis fish ";
	if(temperament)
		output << "does not";
	else
		output << "does";
	output << " play well with others.";

	output << std::endl << "\tThis is a ";
	if(freshwater)
		output << "freshwater";
	else
		output << "saltwater";
	output << " fish and prefer water temperatures between " << temperatureToString(preferredTemp) << ".\n\tTheir tank is currently at a " << tankCleanliness << '/' << MAX_FILTH << ".";
	output << std::endl << "\tThey are ";
	if(!hungry)
		output << "not";
	output << " hungry, and appear to be a " << diseaseToString(diagnosis) << ".\n";

	output << "Physical Traits: ";
	if(physicalTraits.empty())
		output << std::endl << "\tNothing of note.\n";
	else
	{
		int i = 0;
		for(auto it: physicalTraits)
		{
			++i;
			output << std::endl << '\t' << i <<". " << it;
		}
	}


}

// Assist function
void Fish :: deIncrementClean()
{
	--tankCleanliness;
	if(tankCleanliness < 0)
		tankCleanliness = 0;
}

std::string Fish :: diseaseToString(const disease & sick) const
{
	std::string buffer = "error";

	switch(sick)
	{
		case healthy:
			buffer = "healthy fish!";
			break;
		case dropsy:
			buffer = "fish with dropsy symptoms. We recommend administering medicine.";
			break;
		case cotton_mouth:
			buffer = "fish with cotton mouth symptoms. We recommend administering medicine.";
			break;
		case fin_rot:
			buffer = "fish with fin rot symptoms. We recommend administering medicing.";
			break;
	}

	return buffer;
}

std::string Fish :: temperatureToString(const int & num) const
{
	std::string buffer = "error";

	if(num >= 0 && !temperatures.empty() && num < (int) temperatures.size())
	{
		buffer = std::to_string(temperatures.at(num).first) + " - " + std::to_string(temperatures.at(num).second);
	}

	return buffer;
}

std::istream & Fish :: load(std::istream & input)
{
	if(input)
	{
		bool fresh;
		input >> fresh;
		
		if(!input.eof())
		{
			freshwater = fresh;
			input.ignore(1, ';');
			
			input >> hungry;
			input.ignore(1, ';');
			
			int clean;
			input >> clean;
			setTankCleanliness(clean);
			input.ignore(1, ';');
		
			int health;
			input >> health;
			setDiagnosis(health);
			input.ignore(1, ';');
			
			int temperature;
			input >> temperature;
			setPreferredTemp(temperature);
			input.ignore(1, '\n');
		}
	}
	return input;
}



