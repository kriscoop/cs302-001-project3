/*
	Kristian
	This file basically is a collection of user interaction helper functions.
*/
#include "CMD.h"

bool validInput()
{
	if(!std::cin)
	{
		std::cin.clear();
		std::cin.ignore(MAX_STRING, '\n');
		return false;
	}
	else
		return true;
}


int getInt()
{
	int data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput());

	return data;
}

int getInt(const int min, const int max)
{
	int data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput() || (data < min && data > max));

	return data;
}

long getLong()
{
	long data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput());

	return data;
}

long getLong(const int min, const int max)
{
	long data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput() || (data < min && data > max));

	return data;
}

float getFloat()
{
	float data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput());

	return data;
}

float getFloat(const float min, const float max)
{
	float data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput() || (data < min && data > max));

	return data;
}

double getDouble()
{
	double data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput());

	return data;
}

double getDouble(const double min, const double max)
{
	double data = 0;

	do
	{
		std::cin >> data;
		std::cin.ignore(MAX_STRING, '\n');
	}
	while(!validInput() || (data < min && data > max));

	return data;
}

std::string getText()
{
	std::string buffer;

	do
	{
		std::cin.clear();
		getline(std::cin, buffer);
	}
	while(!validInput());

	return buffer;
}


void getText(char buf[])
{
	do
	{
		std::cin.clear();
		std::cin.get(buf, MAX_STRING);
		std::cin.ignore(MAX_STRING, '\n');
		
	}
	while(!validInput());
}


char getChar()
{
	char option;
	std::cin >> option;
	std::cin.ignore(MAX_STRING, '\n');
	
	option = tolower(option);
	return option;
}


bool yesNo(char& option)
{
	do
	{
		option = getChar();
		option = tolower(option);
	}
	while(option != 'n' && option != 'y');
	
	if(option == 'y')
		return true;
	else
		return false;
}



